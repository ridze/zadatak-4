import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

let previousField = null;
let busy = false;

const App = ({ fields, foundMatch, toggleActive }) => {
	const handleDispatch = (event, id, value) => {
		toggleActive(id);
		if (previousField && previousField.value === value) {
			foundMatch(value);
			previousField = null;
		} else if (previousField) {
			busy = true;
			setTimeout(() => {
				busy = false;
				toggleActive(id);
				toggleActive(previousField.id);
				previousField = null;
			}, 1500);
		} else {
			previousField = { id, value };
		}
	};
	const game = fields.toJS().map((field) => {
		const {
			matched,
			active,
			value,
			id,
		} = field;
		const classes = ['gameField', matched ? ' matched' : null, active ? ' active' : null].join('');
		return (
			<div
				className={classes}
				value={value}
				onClick={matched || active || busy ? null : (e => handleDispatch(e, id, value))}
				onKeyUp={matched || active || busy ? null : (e => handleDispatch(e, id, value))}
				id={id}
				key={id}
			>
				{active ? value : null}
			</div>
		);
	});
	return (
		<div className="App">
			<div id="game">
				{ game }
			</div>
		</div>
	);
};

App.propTypes = {
	fields: ImmutablePropTypes.listOf(
		ImmutablePropTypes.contains({
			value: PropTypes.number.isRequired,
			id: PropTypes.number.isRequired,
			matched: PropTypes.bool.isRequired,
			active: PropTypes.bool.isRequired,
		})
	).isRequired,
	foundMatch: PropTypes.func.isRequired,
	toggleActive: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
	fields: state.fields,
});

const mapDispatchToProps = dispatch => ({
	foundMatch: (value) => {
		dispatch({ type: 'FOUND_MATCH', value });
	},
	toggleActive: (id) => {
		dispatch({ type: 'TOGGLE_ACTIVE', id });
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
