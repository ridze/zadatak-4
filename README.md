# zadatak-4

## Author:

Stefan Đorđević

## Run Commands:
```
run program:

npm start
```
```
run linter:

npm run lint
```
## File List:
```
.:

.eslintrc.json

package.json

README.md

./public

./src

```
```
./src:

index.js

index.css

App.js

./reducers

```
```
./src/reducers:

rootReducer.js

```
```
./public:

favicon.ico

index.html

manifest.json

```


