const { fromJS } = require('immutable');

const randomTo = a => Math.floor(Math.random() * (a + 1));

const fields = [];
for (let i = 0; i < 16; i += 2) {
	fields[i] = { value: i, matched: false, active: false };
	fields[i + 1] = { value: i, matched: false, active: false };
}

const mixedFields = [];
for (let i = 0; i < 16; i += 1) {
	const index = randomTo(15 - i);
	mixedFields[i] = fields[index];
	mixedFields[i].id = i;
	fields.splice(index, 1);
}

const initState = {
	fields: fromJS(mixedFields),
};

const rootReducer = (state = initState, action) => {
	if (action.type === 'FOUND_MATCH') {
		const newState = {
			fields: state.fields.map(field => (field.get('value') === action.value
				? field.set('matched', true).set('active', true)
				: field)),
		};
		return newState;
	}
	if (action.type === 'TOGGLE_ACTIVE') {
		const newState = {
			fields: state.fields.map(field => (field.get('id') === action.id
				? field.set('active', !field.get('active'))
				: field)),
		};
		return newState;
	}
	return state;
};

export default rootReducer;
